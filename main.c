/** Libreria MCC*/

#include "mcc_generated_files/mcc.h"

/** Definición de variables globales y constantes*/
#define TRUE 1
#define FALSE 0
#define MAX_VEL 3000
#define MAX_VUELTAS 408
#define FS 30000 //Frecuencia de muestreo 1/periodo de muestreo NO utilizadao
#define ANGLE 90

/** Variables globales para control del enconder*/
int encoder_resultA,encoder_oldA,encoder_resultB,encoder_oldB;
bool flag_encoder,flag_rise;
int count_rise;

/** Definición estados */
typedef enum {
    /** Estados control velocidad*/
    START,
    PRINT,
    /** Estados control por histeresis*/
    WAIT,
    COUNTER_CW_FORCE,
    CW_FORCE,    
    COUNTER_CW,
    CW
} State;

void printfUSART(double,char);
void updateDataWait(void);
/*
                         Main application
 */
void main(void)
{
    // initialize the device
    SYSTEM_Initialize();
   INTERRUPT_GlobalInterruptEnable();
   
   //InicializaciÃ³n de variables globales
   encoder_oldA=FALSE;
   encoder_resultA=FALSE;
   encoder_oldB=FALSE;
   encoder_resultB=FALSE;
   count_rise=0;
   flag_encoder=FALSE;
   flag_rise=FALSE;
   //Variables de control dentro del main
   int angle_count=0;
   int conteo_delay=0;
   int countDelay_spin=0;   
   int angle_low_limit = ANGLE-5;
   int angle_high_limit = ANGLE+5;
   IO_RA4_SetHigh();
   //** Estado inicial ****//
//   State currentState = WAIT;
   State currentState = START;
   /**
    * Variables de control para la velocidad y encendido leds
    */
   long double speed = 0;
   long acumulator = 0;
   /** Inicio del ciclo*/
   while (1)
    {
        switch (currentState)
        {
 /***************** Caso de uso para medir velocidad ******************/
 /**
  * Estado START
  * En este estado el 
 */
            case START:
            if(flag_encoder==TRUE){
                flag_encoder=FALSE;
                /* Esta variable contará las veces que hace interrupciones*/
                acumulator++;
                /** Valida un pulso*/
                if(encoder_resultA == TRUE && encoder_oldA == FALSE){
                        count_rise++; 
                        if(count_rise>MAX_VUELTAS){
                            currentState = PRINT;
                            /** Se multiplica por 330 dado que el TImer0 Esta configurado
                            para interrumpir cada 330ms, */
                            acumulator=acumulator*330         
                            /* Se multiplica por 1000 dado que al divir en el tiempo al ser
                            ms el 10E3 pasa para arriba a multiplicar */             
                            speed=(count_rise*1000)/acumulator;
                            acumulator=0;
                            count_rise=0;
                        }
                }//Derecha
            }
            break;
             
            case PRINT:
                printfUSART(speed,'F');
                currentState = START;
                break;

/*********************  Caso de uso para control por histeresis ********************************************/
            case WAIT :
            if(flag_encoder==TRUE){
                flag_encoder=FALSE;
                if(encoder_resultA == TRUE && encoder_oldA == FALSE){
                    if(encoder_resultB == TRUE){
                        count_rise++;
                        angle_count=(count_rise*36)/11;
                        if(count_rise>110){
                            count_rise=0;
                        }
                    }
                }//Derecha
                if(encoder_resultB == TRUE && encoder_oldB == FALSE){
                   if(encoder_resultA == TRUE){
                    count_rise--;
                    angle_count=(count_rise*36)/11;
                    if(count_rise<=0){
                        count_rise=110;
                    }
                  }
                }//Izquierda
            }
            //LlegÃ³ al angulo deseado
            if(angle_count>= angle_low_limit && angle_high_limit>= angle_count){
                currentState = WAIT;
                IO_RA2_SetLow();//clock wise 
                IO_RA3_SetLow();//anti clock wise
                IO_RA4_SetLow();//Led
            }
            else
            {//No esta en posicion
                if(angle_count>angle_high_limit){
                    TMR1_WriteTimer(0);
                    currentState = CW_FORCE;
                  }
                if(angle_count<angle_low_limit){
                    TMR1_WriteTimer(0);
                    currentState = COUNTER_CW_FORCE; 
                }
            }

        break;
        
            case COUNTER_CW_FORCE: 
                if(flag_encoder==TRUE){
                    flag_encoder=FALSE;
                    if(encoder_resultA == TRUE && encoder_oldA == FALSE){
                        if(encoder_resultB == TRUE){
                            count_rise++;
                            angle_count=(count_rise*36)/11;
                            if(count_rise>110){
                                count_rise=0;
                            }
                        }
                    }//Derecha
                    if(encoder_resultB == TRUE && encoder_oldB == FALSE){
                       if(encoder_resultA == TRUE){
                        count_rise--;
                        angle_count=(count_rise*36)/11;
                        if(count_rise<=0){
                            count_rise=110;
                        }
                      }
                    }//Izquierda
                }
                if(TMR1_WriteTimer>1000){
                    conteo_delay++;
                    TMR1_WriteTimer(0);
                }
                if(conteo_delay>10000){
                    countDelay_spin++;
                    conteo_delay=0;
                }
                if(countDelay_spin>10){
                    countDelay_spin=0;
                    currentState= COUNTER_CW;
                    TMR1_WriteTimer(0);
                    printfUSART((angle_count), 'F');
                    break;
                }
                currentState = COUNTER_CW_FORCE;
            break;        
        
            case CW_FORCE :     
                if(flag_encoder==TRUE){
                    flag_encoder=FALSE;
                    if(encoder_resultA == TRUE && encoder_oldA == FALSE){
                        if(encoder_resultB == TRUE){
                            count_rise++;
                            angle_count=(count_rise*36)/11;
                            if(count_rise>110){
                                count_rise=0;
                            }
                        }
                    }//Derecha
                    if(encoder_resultB == TRUE && encoder_oldB == FALSE){
                       if(encoder_resultA == TRUE){
                        count_rise--;
                        angle_count=(count_rise*36)/11;
                        if(count_rise<=0){
                            count_rise=110;
                        }
                      }
                    }//Izquierda
                }
                if(TMR1_WriteTimer>1000){
                    conteo_delay++;
                    TMR1_WriteTimer(0);
                }
                if(conteo_delay>10000){
                    countDelay_spin++;
                    conteo_delay=0;
                }
                if(countDelay_spin>10){
                    countDelay_spin=0;
                    currentState= CW;
                    TMR1_WriteTimer(0);
                    printfUSART((angle_count), 'F');
                    break;
                }
                currentState = CW_FORCE;              
            break;
            
            case COUNTER_CW:
                if(flag_encoder==TRUE){
                    flag_encoder=FALSE;
                    if(encoder_resultA == TRUE && encoder_oldA == FALSE){
                        if(encoder_resultB == TRUE){
                        count_rise++;
                        angle_count=(count_rise*36)/11;
                        }   
                        if(count_rise>110){
                            count_rise=0;
                        }
                    }
                }
                if(angle_count<angle_low_limit){
                    if(TMR1_WriteTimer>1000){
                        conteo_delay++;
                        TMR1_WriteTimer(0);
                    }
                    if(conteo_delay>10000){
                       IO_RA2_SetHigh();
                       IO_RA4_SetHigh();//Led
                    }
                    if(conteo_delay>10100){
                       IO_RA2_SetLow(); 
                       conteo_delay=0;
                    }
                    currentState = COUNTER_CW;
                }else
                {
                    printfUSART((angle_count), 'F');
                    currentState= WAIT;
                }
            break;
            
            case CW:
                 if(flag_encoder==TRUE){
                    flag_encoder=FALSE;
                    if(encoder_resultB == TRUE && encoder_oldB == FALSE){
                       if(encoder_resultA == TRUE){
                        count_rise--;
                        if(count_rise<0){
                            count_rise=110;
                        }
                        angle_count=(count_rise*36)/11;
                       }
                    }
                }
                 if(angle_count>angle_high_limit){
                    if(TMR1_WriteTimer>1000){
                        conteo_delay++;
                        TMR1_WriteTimer(0);
                    }
                    if(conteo_delay>10000){
                       IO_RA3_SetHigh();
                       IO_RA4_SetHigh();//Led
                    }
                    if(conteo_delay>10100){
                       IO_RA3_SetLow(); 
                       conteo_delay=0;
                    }
                    currentState = CW;
                }else{
                     printfUSART((angle_count), 'F');
                    currentState= WAIT;
                }
            break;
                        
        default:
            break;
        }/** Swithc */
    }/*While*/
}/*Main*/

/*  Funcion printfUSART()
 *  Funcion para enviar datos por serial, 
 *  @param (double)double se recibe un dato de double que sirve para mostrar el angulo y el conteo de pulsos
 *  @paran (char) res Se recibe un char para enviar un Si o No 
 *  
 * si se quiere solo mostrar un numero, se enviar el numero y un caracter F mayuscula.(232,'F)
 * si se quiere mostrar solo texto se enviar un 500 numero y el caracter deseado (499,'s)
 */
void printfUSART(double dato,char res){
    dato=dato+1;
    if(dato != 500){
        /*Trasmite un numero si es 500 quiere decir que no deberÃ­a entrar*/
        if(EUSART_is_tx_ready()){//Trasmision por USART
            EUSART_Write((char)((int)(dato)/100)+48);
            EUSART_Write((char)((int)((dato)/10)%10)+48);
            EUSART_Write((char)((int)(dato)%10)+48);
            EUSART_Write('\n');                        
            EUSART_Write('\r');
        }
    }
    if(res != 'F'){
        /* Trasmite por USAER una Palabra si recibe s imprime SÃ­ y sino imprime No 
         * si recibe F no imprime palabra
         */
        if(EUSART_is_tx_ready()){//Trasmision por USART
            EUSART_Write(res);
            if(res == 's'){
                EUSART_Write((char)(73));
            }else{
                EUSART_Write((char)(79));
            }
            EUSART_Write('\n');                        
            EUSART_Write('\r');
        }        
    }
}